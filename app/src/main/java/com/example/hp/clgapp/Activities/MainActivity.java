package com.example.hp.clgapp.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.hp.clgapp.BodyModel;
import com.example.hp.clgapp.Feed;
import com.example.hp.clgapp.Formatt;
import com.example.hp.clgapp.Interface.MyApi;
import com.example.hp.clgapp.MainFeed;
import com.example.hp.clgapp.Polls;
import com.example.hp.clgapp.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    private static String TAG = "MainActivity";
    public LinearLayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    private float[] ydata = {75f, 20f, 5f};
    private String[] xdata = {"A", "B", "C"};
    PieChart pieChart;
    int colorgreen = Color.parseColor("#2dcc70");
    int colorred = Color.parseColor("#EC7148");
    int coloryellow = Color.parseColor("#f1c40f");
    MyApi myapi;
    private List<Polls> datalist;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Log.d(TAG, "onCreate: start");
        pieChart = (PieChart) findViewById(R.id.pchart);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setDescription(null);
        pieChart.setHoleRadius(0);
        pieChart.setUsePercentValues(true);
        pieChart.setTouchEnabled(false);
        pieChart.setDrawEntryLabels(true);
        pieChart.setRotationEnabled(false);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        getResults();
        addDataset();
    }

    private void addDataset() {

        ArrayList<PieEntry> yEntrys = new ArrayList<>();
        ArrayList<String> xEntrys = new ArrayList<>();

        for (int i = 0; i < ydata.length; i++) {
            yEntrys.add(new PieEntry(ydata[i], i));

        }

        for (int i = 1; i < xdata.length; i++) {
            xEntrys.add(xdata[i]);

        }

        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setSelectionShift(6f);
        pieDataSet.setValueLinePart1OffsetPercentage(60.f);
        pieDataSet.setValueLinePart1Length(1f);
        pieDataSet.setValueLinePart2Length(2f);
        pieDataSet.setValueTextSize(15f);
        pieDataSet.setValueLineColor(Color.WHITE);
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);


        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(colorgreen);
        colors.add(colorred);
        colors.add(coloryellow);
        pieDataSet.setColors(colors);


        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.NONE);
        legend.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);

        PieData pieData = new PieData(pieDataSet);
        pieData.setValueTextColor(Color.WHITE);
        pieData.setValueFormatter(new Formatt());
        pieChart.setData(pieData);
        pieChart.invalidate();
    }


    private void getResults() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MyApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) //Here we are using the GsonConverterFactory to directly convert json data to object
                .build();

        myapi = retrofit.create(MyApi.class);

        String secParamOfHeader="application/json";

        BodyModel bodyModel=new BodyModel();
        bodyModel.setAppUserID(10);

        Call<MainFeed> call = myapi.getResults(getString(R.string.token),bodyModel,secParamOfHeader);
        call.enqueue(new Callback<MainFeed>() {
            @Override
            public void onResponse(Call<MainFeed> call, Response<MainFeed> response) {
                datalist=response.body().getResponse().getPolls();
                Log.d("connnn",datalist.toString());

                layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                adapter = new RecyclerAdapter(getApplicationContext(), datalist);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<MainFeed> call, Throwable t) {

                Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                Log.d("not",t.toString());

            }
        });

    }


}
