package com.example.hp.clgapp.Interface;

import com.example.hp.clgapp.BodyModel;
import com.example.hp.clgapp.Feed;
import com.example.hp.clgapp.MainFeed;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by osr on 8/2/18.
 */

public interface MyApi {

        String BASE_URL = "http://18.221.244.184:4005/lineguru/api/";

    @POST("v1/poll/userPoll")
    Call<MainFeed> getResults(@Header("accessToken") String contentType , @Body BodyModel bodyModel, @Header("Content-Type") String contentType1);

    // Call<Feed> createUser(@Header("Content-Type") String contentType, @Body CreateUs)
}