package com.example.hp.clgapp;

import com.google.gson.annotations.SerializedName;

/**
 * Created by osr on 9/2/18.
 */

public class MainFeed {

    @SerializedName("status")
    int status;
    @SerializedName("response")
    Feed response;
    @SerializedName("time")
    String time;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Feed getResponse() {
        return response;
    }

    public void setResponse(Feed response) {
        this.response = response;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



}
