package com.example.hp.clgapp;

import com.google.gson.annotations.SerializedName;

/**
 * Created by osr on 8/2/18.
 */

public class Polls {

    @SerializedName("poll_id")
    String poll_id;
    @SerializedName("match_date")
    String match_date;
    @SerializedName("teams")
    String teams;
    @SerializedName("winTeam")
    String winTeam;
    @SerializedName("myPoll")
    String myPoll;

    public String getPoll_id() {
        return poll_id;
    }

    public void setPoll_id(String poll_id) {
        this.poll_id = poll_id;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getTeams() {
        return teams;
    }

    public void setTeams(String teams) {
        this.teams = teams;
    }

    public String getWinTeam() {
        return winTeam;
    }

    public void setWinTeam(String winTeam) {
        this.winTeam = winTeam;
    }

    public String getMyPoll() {
        return myPoll;
    }

    public void setMyPoll(String myPoll) {
        this.myPoll = myPoll;
    }



}
