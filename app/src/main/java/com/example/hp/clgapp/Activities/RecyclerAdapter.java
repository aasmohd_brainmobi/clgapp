package com.example.hp.clgapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp.clgapp.Polls;
import com.example.hp.clgapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;



public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    Context context;
    View view;
    private List<Polls> my_data;


    public RecyclerAdapter(Context context, List<Polls> my_data) {

        this.context = context;
        this.my_data = my_data;
    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapterdata, null);

        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);
        return recyclerViewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        String TimestToDate = getTStoDate(my_data.get(position).getMatch_date());

        holder.date.setText(TimestToDate);
        holder.teams.setText(my_data.get(position).getTeams());
        holder.won.setText(my_data.get(position).getWinTeam());
        holder.mypoll.setText(my_data.get(position).getMyPoll());
        if(position%2==0) {
            holder.mypoll.setTextColor(Color.RED);
        }
        else  holder.mypoll.setTextColor(Color.YELLOW);
    }

    @Override
    public int getItemCount() {

        return my_data.size();

    }

    public String getTStoDate(String tsValue) {

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        Long num = Long.parseLong(tsValue);
        cal.setTimeInMillis(num);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }








    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView date, teams, won, mypoll;


        public RecyclerViewHolder(View View) {
            super(View);

           date = (TextView) View.findViewById(R.id.date);
            teams = (TextView) View.findViewById(R.id.teams);
            won = (TextView) View.findViewById(R.id.won);
            mypoll = (TextView) View.findViewById(R.id.mypoll);




        }
    }




}