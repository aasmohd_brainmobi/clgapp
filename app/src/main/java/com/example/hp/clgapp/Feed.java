package com.example.hp.clgapp;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by osr on 8/2/18.
 */

public class Feed {


    @SerializedName("correctPoll")
    int correctPoll;
    @SerializedName("incorrectPoll")
    int incorrectPoll;
    @SerializedName("pendingPoll")
    int pendingPoll;
    @SerializedName("polls")
    public ArrayList<Polls> polls;

    public int getCorrectPoll() {
        return correctPoll;
    }

    public void setCorrectPoll(int correctPoll) {
        this.correctPoll = correctPoll;
    }

    public int getIncorrectPoll() {
        return incorrectPoll;
    }

    public void setIncorrectPoll(int incorrectPoll) {
        this.incorrectPoll = incorrectPoll;
    }

    public int getPendingPoll() {
        return pendingPoll;
    }

    public void setPendingPoll(int pendingPoll) {
        this.pendingPoll = pendingPoll;
    }

    public ArrayList<Polls> getPolls() {
        return polls;
    }

    public void setPolls(ArrayList<Polls> polls) {
        this.polls = polls;
    }


}
